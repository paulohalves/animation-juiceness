﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    [SerializeField] private Transform _target = null;
    [SerializeField] private Transform _neck = null;
    [SerializeField] private Transform _camera = null;
    [Space]
    [SerializeField] private float _cameraMovementSpeed = 2.0f;
    [SerializeField] private Vector2 _cameraVerticalLimits = default;
    [SerializeField] private Vector2 _cameraScrollLimits = default;

    private Vector2 _mouseInput;
    private float _cameraAngle;
    private Vector3 _offset;
    private float _zoom;

    private void Awake()
    {
        _offset = _camera.localPosition;
    }

    private void Start()
    {
        _cameraAngle = _cameraAngle + Mathf.DeltaAngle(_cameraAngle, _neck.transform.localEulerAngles.x);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            _mouseInput.x = -Input.GetAxis("Mouse X");
            _mouseInput.y = Input.GetAxis("Mouse Y");

            transform.Rotate(Vector3.up, _mouseInput.x * _cameraMovementSpeed * Time.deltaTime);
            _cameraAngle = Mathf.Clamp(_cameraAngle + _mouseInput.y * _cameraMovementSpeed * Time.deltaTime, _cameraVerticalLimits.x, _cameraVerticalLimits.y);
            _neck.transform.localEulerAngles = new Vector3(_cameraAngle, 0, 0);
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (Input.mouseScrollDelta.y != 0)
        {
            _zoom = Mathf.Clamp(_zoom + Input.mouseScrollDelta.y, _cameraScrollLimits.x, _cameraScrollLimits.y);            
            _camera.localPosition = _offset + Vector3.forward * _zoom;
        }
    }

    private void LateUpdate()
    {
        transform.position = _target.transform.position;
    }
}
