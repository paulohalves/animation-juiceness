﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSystem : MonoBehaviour
{
    [SerializeField] GameObject _staticCamera = null;
    [SerializeField] GameObject _followCamera = null;

    private bool m_FollowCamera = false;

    public bool FollowCamera
    {
        get => m_FollowCamera;
        set
        {
            m_FollowCamera = value;
            _followCamera.SetActive(value);
            _staticCamera.SetActive(!value);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleCamera();
        }
    }

    public void ToggleCamera()
    {
        FollowCamera = !FollowCamera;
    }
}
