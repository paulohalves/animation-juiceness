﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    [SerializeField] private AnimationHandler _animationHandler = null;
    [Space]
    [SerializeField] private ParticleSystem _jumpParticle = null;
    [SerializeField] private ParticleSystem _landParticle = null;
    [SerializeField] private ParticleSystem _starsParticle = null;
    [SerializeField] private ParticleSystem _hatOffParticle = null;
    [SerializeField] private ParticleSystem _hatOnParticle = null;
    [SerializeField] private ParticleSystem _pokeLeftParticle = null;
    [SerializeField] private ParticleSystem _pokeRightParticle = null;
    [Space]
    [SerializeField] private Animation _animation = null;
    [SerializeField] private AnimationClip _jumpAnim = null;
    [SerializeField] private AnimationClip _jellyLAnim = null;
    [SerializeField] private AnimationClip _jellyRAnim = null;
    [SerializeField] private AnimationClip _stuntAnim = null;
    [Space]
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private AudioClip _jumpAudioClip = null;
    [SerializeField] private AudioClip _jellyAudioClip = null;
    [SerializeField] private AudioClip _stuntAudioClip = null;
    [SerializeField] private AudioClip _hatOnAudioClip = null;
    [SerializeField] private AudioClip _hatOffAudioClip = null;

    private FeetHandler _feet = null;
    private CubeHandler _cube = null;
    private HatHandler _hat = null;

    private void Awake()
    {
        _feet = FindObjectOfType<FeetHandler>();
        _cube = FindObjectOfType<CubeHandler>();
        _hat = FindObjectOfType<HatHandler>();

        _animationHandler.OnJump += HandlerOnJump;
        _animationHandler.OnLand += HandleOnLand;
        _animationHandler.OnHatOff += HandleOnHatOff;
        _animationHandler.OnHatOn += HandleOnHatOn;
        _animationHandler.OnMiddle += HandleOnMiddle;
        _animationHandler.OnJellyLeft += HandleOnJellyLeft;
        _animationHandler.OnJellyRight += HandleOnJellyRight;
    }

    public void PlayJump()
    {
        if (_animation.isPlaying)
        {
            return;
        }
        _audioSource.PlayOneShot(_jumpAudioClip);
        _animation.Play(_jumpAnim.name);
    }

    public void PlayPokeLeft()
    {
        if (_animation.isPlaying)
        {
            return;
        }
        _audioSource.PlayOneShot(_jellyAudioClip);
        _animation.Play(_jellyLAnim.name);
    }

    public void PlayPokeRight()
    {
        if (_animation.isPlaying)
        {
            return;
        }
        _audioSource.PlayOneShot(_jellyAudioClip);
        _animation.Play(_jellyRAnim.name);
    }

    public void PlayStunt()
    {
        if (_animation.isPlaying)
        {
            return;
        }
        _audioSource.PlayOneShot(_stuntAudioClip);
        _animation.Play(_stuntAnim.name);
    }

    public void SetTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
        _audioSource.pitch = timeScale;
    }

    private void HandleOnMiddle()
    {
        _starsParticle.Play();
    }

    private void HandleOnHatOn()
    {
        _audioSource.PlayOneShot(_hatOnAudioClip);
        _hatOnParticle.Play();
    }

    private void HandleOnHatOff()
    {
        _audioSource.PlayOneShot(_hatOffAudioClip);
        _hatOffParticle.Play();
    }

    private void HandleOnLand()
    {
        _landParticle.transform.position = _feet.transform.position;
        _landParticle.Play();
    }

    private void HandlerOnJump()
    {
        _jumpParticle.transform.position = _feet.transform.position;
        _jumpParticle.Play();
    }

    private void HandleOnJellyRight()
    {
        _pokeRightParticle.Play();
    }

    private void HandleOnJellyLeft()
    {
        _pokeLeftParticle.Play();
    }
}
