﻿using UnityEngine;

public class ParticleFollower : MonoBehaviour
{
    [SerializeField] private Transform _follow = null;

    private void LateUpdate()
    {
        if (_follow)
        {
            transform.position = _follow.transform.position;
            transform.rotation = _follow.transform.rotation;
        }
    }
}
