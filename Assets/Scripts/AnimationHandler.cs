﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    public delegate void EventHandler();
    public event EventHandler OnJump;
    public event EventHandler OnHatOff;
    public event EventHandler OnMiddle;
    public event EventHandler OnLand;
    public event EventHandler OnHatOn;
    public event EventHandler OnJellyLeft;
    public event EventHandler OnJellyRight;

    private void Jump()
    {
        OnJump?.Invoke();
    }

    private void HatOff()
    {
        OnHatOff?.Invoke();
    }

    private void Middle()
    {
        OnMiddle?.Invoke();
    }

    private void Land()
    {
        OnLand?.Invoke();
    }

    private void HatOn()
    {
        OnHatOn?.Invoke();
    }

    private void JellyLeft()
    {
        OnJellyLeft?.Invoke();
    }

    private void JellyRight()
    {
        OnJellyRight?.Invoke();
    }
}
