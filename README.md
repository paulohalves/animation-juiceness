# Animation Juiceness

Por Paulo Henrique Alves de Deus.

As particulas foram adquiridas na Unity Asset Store. 
Os efeitos sonoros são free, adquiridos na Web. 
Modelos, animações e montagem sonora são autorais.

Comandos:

Use os botões na interface para disparar as animações.
Ao trocar para a câmera livre, segure com o botão direito do mouse e mova-o para visão livre.
Use o scroll do mouse para mudar o zoom da câmera livre.